import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  displayedColumns: string[] = ['firstname', 'lastname', 'email', 'address', 'gender', 'mobile'];
  usersdata:any=[];
  dataSource:any;
  constructor(private router : Router) { }

  ngOnInit(): void {
    var data = JSON.parse(localStorage.getItem("userdata"));
    this.usersdata = data;
    console.log(this.usersdata);

  }

  logout(){
    this.router.navigateByUrl("login");
  }

}
