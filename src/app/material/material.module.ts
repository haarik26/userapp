import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,MatCardModule,MatInputModule,MatFormFieldModule,MatButtonModule,MatRadioModule,MatToolbarModule,
    MatIconModule,MatTableModule,MatPaginatorModule
  ],
  exports:[
    MatCardModule,MatInputModule,MatFormFieldModule,MatButtonModule,MatRadioModule,MatToolbarModule,
    MatIconModule,MatTableModule,MatPaginatorModule
  ]
})
export class MaterialModule { }
