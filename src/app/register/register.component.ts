import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup , Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from './mustmatch';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  submitted = false;
  registerform:FormGroup;
  usersdata:any=[];

  constructor(private formbuilder:FormBuilder , private router :Router) { }

  ngOnInit(): void {
    var data = JSON.parse(localStorage.getItem("userdata"));
    console.log(data);
    if(data != null){
      this.usersdata.push(data);
    }
    this.registerform = this.formbuilder.group({
      firstname:["",Validators.required],
      lastname:["",Validators.required],
      email:["",Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-zA-Z0-9.-]+$')])],
      mobile:["",Validators.compose([Validators.required,Validators.minLength(10) ,Validators.pattern('[1-9 ]*')])],
      gender:[""],
      address:[""],
      password:["",Validators.compose([Validators.required,Validators.minLength(8)])],
      cpassword:["",Validators.required]
    },{
      validator: MustMatch('password', 'cpassword')
    });
  }

  register(){
    console.log(this.usersdata);
    if(this.registerform.valid){
      this.usersdata.push(this.registerform.value);
      console.log(this.registerform.value);
      localStorage.setItem("userdata", JSON.stringify(this.usersdata));
        alert("Registered successfully!");
        this.router.navigateByUrl("login");
    }
  }

}
