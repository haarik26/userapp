import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup , Validators } from '@angular/forms';
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  submitted = false;
  loginform:FormGroup;
  usersdata:any=[];
  constructor(private formbuilder:FormBuilder , private router :Router) { }

  ngOnInit(): void {
    var data = JSON.parse(localStorage.getItem("userdata"));
    if(data != null){
      this.usersdata = data;
    }
    console.log(this.usersdata.length);
    this.loginform = this.formbuilder.group({
      username:["",Validators.required],
      Password:["",Validators.required]
    });
  }

  login(){
    var message;
    if(this.loginform.valid){
      for(var i=0; i<this.usersdata.length;i++){
        if(this.loginform.value.username == this.usersdata[i].email && 
            this.loginform.value.Password == this.usersdata[i].password){
            message = "Login success!"
          this.router.navigateByUrl("welcome");
          localStorage.setItem("login","yes");
        }else{
          if(message != "Login success!"){
            message = "User Credentials not found!"
          }
        }
      }
      alert(message);
    }
  }

  register(){
    this.router.navigateByUrl("register");
  }
}
